import { createSlice, PayloadAction } from '@reduxjs/toolkit';

interface TestReducer {
  isTest: boolean;
}

const initialState: TestReducer = {
  isTest: false,
};

const test = createSlice({
  name: 'test',
  initialState,
  reducers: {
    setTest(state, action: PayloadAction<boolean>) {
      state.isTest = action.payload;
    },
  },
});

export const { setTest } = test.actions;
export default test.reducer;
