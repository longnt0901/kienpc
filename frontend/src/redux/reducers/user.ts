import { createSlice, PayloadAction } from '@reduxjs/toolkit';

interface UserInfo {
  username: string;
}
interface UserReducer {
  isLogged: boolean;
  user: UserInfo | null;
}

const initialState: UserReducer = {
  isLogged: false,
  user: null
};

const user = createSlice({
  name: 'user',
  initialState,
  reducers: {
    setIsLogged(state, action: PayloadAction<boolean>) {
      state.isLogged = action.payload;
      return state;
    },
    setUserInfo(state, action: PayloadAction<UserInfo>) {
      state.user = action.payload;
      return state;
    }
  },
});

export const { setIsLogged } = user.actions;
export default user.reducer;
