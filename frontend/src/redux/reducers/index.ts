import { combineReducers } from '@reduxjs/toolkit';
import test from './test';
import user from './user';

const rootReducers = combineReducers({
  test,
  user
});

export default rootReducers;

export type RootState = ReturnType<typeof rootReducers>
