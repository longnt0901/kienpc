export interface UserDataList {
  key: React.Key;
  username: string;
  role: string;
  employee: string;
  disabled: boolean;
}
