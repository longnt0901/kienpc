export interface EmployeeDataList {
  key: React.Key;
  fullname: string;
  mobile: string;
  age: string;
  address: string;
  warehouses: string;
  departments: string;
  created_at: string;
  updated_at: string;
  disabled: boolean;
}
