import Rq from '../libs/Req';

const login = (username: string, password: string) => {
  const url = 'login';
  return Rq.post(url, {
    username,
    password,
  });
};

export { login };
