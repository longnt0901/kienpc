import { isMobile } from './Browser';

const toUrl = (str: string): string => {
  let path = '';

  if (isMobile()) {
    path = '';
  }

  path += str;
  return path;
};

export { toUrl };
