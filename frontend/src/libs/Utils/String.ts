const textLimit = (str: string, limit: number): string => {
  return str;
};

const randId = (): string => {
  return Math.random().toString(36).substr(2, 9);
};

export { textLimit, randId };
