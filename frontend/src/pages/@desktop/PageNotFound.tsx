import { Col, Result, Row } from 'antd';
import { useEffect } from 'react';

const PageNotFound = (): JSX.Element => {
  useEffect(() => {
    document.title = 'Trang không tồn tại!';
  }, []);

  return (
    <Row>
      <Col span={24}>
        <Result status="404" title="404" subTitle="Trang không tồn tại!!!" />
      </Col>
    </Row>
  );
};

export default PageNotFound;
