import { useEffect } from 'react';

const Dashboard = (): JSX.Element => {
  useEffect(() => {
    document.title = 'Dashboard';
  }, []);

  return (
    <>Dashboard</>
  );
};

export default Dashboard;
