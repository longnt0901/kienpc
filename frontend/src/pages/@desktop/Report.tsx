import { useEffect } from 'react';

const Report = (): JSX.Element => {
  useEffect(() => {
    document.title = 'Report';
  }, []);

  return (
    <>Report</>
  );
};

export default Report;
