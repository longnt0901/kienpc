import { Button, Card, Modal, Table } from 'antd';
import { useState } from 'react';
import { useEffect } from 'react';
import { UserDataList } from '../../../interfaces/user';
import UserAddForm from './UserAddForm';

const columns = [
  {
    title: 'Tên tài khoản',
    dataIndex: 'username',
    render: (text: string) => text,
  },
  {
    title: 'Quyền',
    dataIndex: 'role',
  },
  {
    title: 'Nhân viên',
    dataIndex: 'employee',
  },
];

const data: UserDataList[] = [];

const rowSelection = {
  onChange: (selectedRowKeys: React.Key[], selectedRows: UserDataList[]) => {
    console.log(
      `selectedRowKeys: ${selectedRowKeys}`,
      'selectedRows: ',
      selectedRows,
    );
  },
  getCheckboxProps: (record: UserDataList) => ({
    disabled: record.disabled,
    username: record.username,
  }),
};

const UserList = (): JSX.Element => {
  const [isOpenModal, setIsOpenModal] = useState(false);

  useEffect(() => {
    // get user list
  }, []);

  const handleOk = () => {
    setIsOpenModal(false);
  };

  const handleCancel = () => {
    setIsOpenModal(false);
  };

  const handleOpenModal = () => {
    setIsOpenModal(true);
  };

  return (
    <>
      <Card
        title="Tài khoản"
        extra={
          <Button onClick={handleOpenModal} type="primary">
            Thêm mới
          </Button>
        }
      >
        <Table
          rowSelection={{
            type: 'checkbox',
            ...rowSelection,
          }}
          columns={columns}
          dataSource={data}
        />
      </Card>

      <Modal
        title="Thêm mới tài khoản"
        visible={isOpenModal}
        onOk={handleOk}
        okText="Đồng ý"
        cancelText="Huỷ bỏ"
        onCancel={handleCancel}
        destroyOnClose={true}
        centered={true}
      >
        <UserAddForm />
      </Modal>
    </>
  );
};

export default UserList;
