import { Col, Layout, Row, Skeleton } from 'antd';
import { Content, Footer, Header } from 'antd/lib/layout/layout';
import { lazy, Suspense, useEffect } from 'react';
import { Route, Switch } from 'react-router-dom';
import '../../assets/styles/@desktop/home.scss';
import Navigation from './@Layout/Navigation';
import Dashboard from './Home/Dashboard';

const Employee = lazy(() => import('../@desktop/Employee'));
const User = lazy(() => import('../@desktop/User'));
const WareHouse = lazy(() => import('../@desktop/WareHouse'));
const Work = lazy(() => import('../@desktop/Work'));
const Settings = lazy(() => import('../@desktop/Settings'));
const Report = lazy(() => import('../@desktop/Report'));
const PageNotFound = lazy(() => import('../@desktop/PageNotFound'));

const Home = (): JSX.Element => {
  useEffect(() => {
    document.title = 'Home';
  }, []);

  return (
    <Layout>
      <Header>
        <Navigation />
      </Header>
      <Content style={{ padding: '0 50px' }}>
        <Row style={{ marginTop: 20 }}>
          <Col span={24}>
            <Suspense fallback={<Skeleton active />}>
              <Switch>
                <Route exact path={'/'} component={Dashboard} />
                <Route path={`/employee`} component={Employee} />
                <Route path={`/user`} component={User} />
                <Route path={`/warehouse`} component={WareHouse} />
                <Route path={`/work`} component={Work} />
                <Route path={`/settings`} component={Settings} />
                <Route path={`/report`} component={Report} />
                <Route path="*" component={PageNotFound} />
              </Switch>
            </Suspense>
          </Col>
        </Row>
      </Content>
      <Footer>Footer</Footer>
    </Layout>
  );
};

export default Home;
