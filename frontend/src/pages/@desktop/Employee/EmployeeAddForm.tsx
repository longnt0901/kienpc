import { Form, Input, Select } from 'antd';
const { Option } = Select;

const EmployeeAddForm = (): JSX.Element => {
  const onFinish = () => {};

  return (
    <Form
      layout="horizontal"
      labelCol={{ span: 6 }}
      wrapperCol={{ span: 18 }}
      name="nest-messages"
      onFinish={onFinish}
    >
      <Form.Item
        name={'username'}
        label="Tên tài khoản"
        rules={[{ required: true, message: 'Tên tài khoản yêu cầu nhập.' }]}
      >
        <Input />
      </Form.Item>
      <Form.Item
        name={'password'}
        label="Mật khẩu"
        rules={[{ required: true, message: 'Mật khẩu yêu cầu nhập.' }]}
      >
        <Input.Password />
      </Form.Item>
      <Form.Item name={'role'} label="Quyền">
        <Select defaultValue="USER">
          <Option value="ADMIN">Admin</Option>
          <Option value="MANAGER">Quản lý xưởng</Option>
          <Option value="USER">Nhân viên</Option>
        </Select>
      </Form.Item>
      <Form.Item name={'employee_id'} label="Nhân viên">
        <Select defaultValue="">
          <Option value="100">Nguyễn Thị Lài</Option>
          <Option value="101">Bùi Văn Quyết</Option>
          <Option value="102">Vũ Đức Việt</Option>
        </Select>
      </Form.Item>
    </Form>
  );
};

export default EmployeeAddForm;
