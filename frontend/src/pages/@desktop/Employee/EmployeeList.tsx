import { Button, Card, Col, Input, Modal, Row, Table } from 'antd';
import { useState } from 'react';
import { useEffect } from 'react';
import { EmployeeDataList } from '../../../interfaces/employee';
import EmployeeAddForm from './EmployeeAddForm';

const { Search } = Input;

const columns = [
  {
    title: 'Tên nhân viên',
    dataIndex: 'fullname',
    render: (text: string) => text,
  },
  {
    title: 'Điện thoại',
    dataIndex: 'mobile',
  },
  {
    title: 'Tuổi',
    dataIndex: 'age',
  },
  {
    title: 'Địa chỉ',
    dataIndex: 'address',
  },
  {
    title: 'Xưởng',
    dataIndex: 'warehouses',
  },
  {
    title: 'Bộ phận',
    dataIndex: 'departments',
  },
  {
    title: 'Ngày tạo',
    dataIndex: 'created_at',
  },
  {
    title: 'Ngày cập nhật',
    dataIndex: 'updated_at',
  },
];

const data: EmployeeDataList[] = [];

const rowSelection = {
  onChange: (
    selectedRowKeys: React.Key[],
    selectedRows: EmployeeDataList[],
  ) => {
    console.log(
      `selectedRowKeys: ${selectedRowKeys}`,
      'selectedRows: ',
      selectedRows,
    );
  },
  getCheckboxProps: (record: EmployeeDataList) => ({
    disabled: record.disabled,
    fullname: record.fullname,
  }),
};

const EmployeeList = (): JSX.Element => {
  const [isOpenModal, setIsOpenModal] = useState(false);

  useEffect(() => {
    // get user list
  }, []);

  const handleOk = () => {
    setIsOpenModal(false);
  };

  const handleCancel = () => {
    setIsOpenModal(false);
  };

  const handleOpenModal = () => {
    setIsOpenModal(true);
  };

  const onSearch = () => {};

  return (
    <>
      <Card
        title="Danh sách Nhân viên"
        extra={
          <>
            <Row gutter={[8, 8]}>
              <Col className="gutter-row" span={16}>
                <Search
                  placeholder="input search text"
                  onSearch={onSearch}
                  enterButton
                />
              </Col>
              <Col className="gutter-row" span={8}>
                <Button onClick={handleOpenModal} type="primary">
                  Thêm mới
                </Button>
              </Col>
            </Row>
          </>
        }
      >
        <Table
          rowSelection={{
            type: 'checkbox',
            ...rowSelection,
          }}
          columns={columns}
          dataSource={data}
        />
      </Card>

      <Modal
        title="Thêm mới Nhân viên"
        visible={isOpenModal}
        onOk={handleOk}
        okText="Đồng ý"
        cancelText="Huỷ bỏ"
        onCancel={handleCancel}
        destroyOnClose={true}
        centered={true}
      >
        <EmployeeAddForm />
      </Modal>
    </>
  );
};

export default EmployeeList;
