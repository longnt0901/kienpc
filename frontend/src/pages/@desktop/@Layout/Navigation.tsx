import { Menu } from 'antd';
import {
  UserOutlined,
  ShopOutlined,
  SettingOutlined,
  TeamOutlined,
  PieChartOutlined,
  HistoryOutlined,
} from '@ant-design/icons';
import SubMenu from 'antd/lib/menu/SubMenu';
import { useHistory } from 'react-router-dom';

const Navigation = (): JSX.Element => {
  const history = useHistory();

  const handleClick = (e: any) => {
    history.push(`${e.key}`);
  };

  return (
    <>
      <Menu mode="horizontal" theme="dark" onClick={handleClick}>
        <SubMenu
          onTitleClick={handleClick}
          key="/warehouse"
          icon={<ShopOutlined />}
          title="Kho"
        >
          <Menu.Item key="/warehouse">Danh sách</Menu.Item>
          <Menu.Item key="/warehouse/import">Nhập hàng</Menu.Item>
          <Menu.Item key="/warehouse/checking">Kiểm hàng</Menu.Item>
          <Menu.Item key="/warehouse/history">Lịch sử</Menu.Item>
        </SubMenu>
        <Menu.Item key="/work" icon={<HistoryOutlined />}>
          Công việc
        </Menu.Item>
        <Menu.Item key="/report" icon={<PieChartOutlined />}>
          Thống kê
        </Menu.Item>
        <Menu.Item key="/settings" icon={<SettingOutlined />}>
          Cấu hình
        </Menu.Item>
        <Menu.Item key="/employee" icon={<UserOutlined />}>
          Nhân viên
        </Menu.Item>
        <Menu.Item key="/user" icon={<TeamOutlined />}>
          Tài khoản
        </Menu.Item>
      </Menu>
    </>
  );
};

export default Navigation;
