import { Skeleton } from 'antd';
import { lazy } from 'react';
import { Suspense, useEffect } from 'react';
import { Route, Switch } from 'react-router-dom';
import PageNotFound from './PageNotFound';

const UserList = lazy(() => import('./User/UserList'));

const User = (): JSX.Element => {
  useEffect(() => {
    document.title = 'Tài khoản';
  }, []);

  return (
    <Suspense fallback={<Skeleton active />}>
      <Switch>
        <Route exact path="/user" component={UserList} />
        <Route path="*" component={PageNotFound} />
      </Switch>
    </Suspense>
  );
};

export default User;
