import { Skeleton } from 'antd';
import { Suspense, useEffect } from 'react';
import { Route, Switch } from 'react-router-dom';
import EmployeeList from './Employee/EmployeeList';
import PageNotFound from './PageNotFound';

const Employee = (): JSX.Element => {
  useEffect(() => {
    document.title = 'Employee';
  }, []);

  return (
    <Suspense fallback={<Skeleton active />}>
      <Switch>
        <Route exact path="/employee" component={EmployeeList} />
        <Route path="*" component={PageNotFound} />
      </Switch>
    </Suspense>
  );
};

export default Employee;
