import { useEffect } from 'react';

const Settings = (): JSX.Element => {
  useEffect(() => {
    document.title = 'Settings';
  }, []);

  return (
    <>Settings</>
  );
};

export default Settings;
