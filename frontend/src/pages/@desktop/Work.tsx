import { useEffect } from 'react';

const Work = (): JSX.Element => {
  useEffect(() => {
    document.title = 'Work';
  }, []);

  return (
    <>Work</>
  );
};

export default Work;
