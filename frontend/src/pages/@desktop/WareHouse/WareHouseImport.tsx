import { useEffect } from 'react';

const WareHouseImport = (): JSX.Element => {
  useEffect(() => {
    document.title = 'WareHouseImport';
  }, []);

  return (
    <>WareHouseImport</>
  );
};

export default WareHouseImport;
