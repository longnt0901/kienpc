import '../../assets/styles/@desktop/login.scss';
import { Button, Checkbox, Form, Input } from 'antd';
import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { setIsLogged } from '../../redux/reducers/user';
// import { login } from '../../services/user';

const Login = (): JSX.Element => {
  const dispatch = useDispatch();
  const onFinish = (values: any) => {
    // login(values.username, values.password).then(response => {
    global.localStorage.setItem('user', 'admin');
    dispatch(setIsLogged(true));
    // }).catch(error => {
    // });
  };

  const onFinishFailed = (errorInfo: any) => {
    console.log('failed', errorInfo);
  };

  useEffect(() => {
    document.title = 'Login';
  }, []);

  return (
    <div className="login">
      <Form
        name="basic"
        layout="vertical"
        initialValues={{ remember: true }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
      >
        <Form.Item
          label="Username"
          name="username"
          rules={[{ required: true, message: 'Please input your username!' }]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Password"
          name="password"
          rules={[{ required: true, message: 'Please enter your password!' }]}
        >
          <Input.Password />
        </Form.Item>

        <Form.Item name="remember" valuePropName="checked">
          <Checkbox>Remember me</Checkbox>
        </Form.Item>

        <Form.Item>
          <Button type="primary" htmlType="submit">
            Submit
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
};

export default Login;
