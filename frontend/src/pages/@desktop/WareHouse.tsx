import { Skeleton } from 'antd';
import { lazy } from 'react';
import { Suspense, useEffect } from 'react';
import { Route, Switch } from 'react-router-dom';
import PageNotFound from './PageNotFound';

const WareHouseList = lazy(() => import('./WareHouse/WareHouseList'));
const WareHouseImport = lazy(() => import('./WareHouse/WareHouseImport'));
const WareHouseChecking = lazy(() => import('./WareHouse/WareHouseChecking'));
const WareHouseHistory = lazy(() => import('./WareHouse/WareHouseHistory'));

const WareHouse = (): JSX.Element => {
  useEffect(() => {
    document.title = 'WareHouse';
  }, []);

  return (
    <Suspense fallback={<Skeleton active />}>
      <Switch>
        <Route exact path="/warehouse" component={WareHouseList} />
        <Route path={`/warehouse/import`} component={WareHouseImport} />
        <Route path={`/warehouse/checking`} component={WareHouseChecking} />
        <Route path={`/warehouse/history`} component={WareHouseHistory} />
        <Route path="*" component={PageNotFound} />
      </Switch>
    </Suspense>
  );
};

export default WareHouse;
