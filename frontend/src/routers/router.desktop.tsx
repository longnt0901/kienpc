import { lazy, Suspense } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import Loading from '../components/@desktop/Loading';
import 'antd/dist/antd.css';
import { useSelector } from 'react-redux';
import { RootState } from '../redux/reducers';

const Home = lazy(() => import('../pages/@desktop/Home'));
const Login = lazy(() => import('../pages/@desktop/Login'));

const Routers = (): JSX.Element => {
  const { isLogged } = useSelector((state: RootState) => state.user);

  return (
    <Suspense fallback={<Loading />}>
      <Switch>
        <Route path="/" component={isLogged ? Home : Login} />
        <Redirect to="/" />
      </Switch>
    </Suspense>
  );
};

export default Routers;
