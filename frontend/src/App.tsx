import { useDispatch } from 'react-redux';
import { isMobile } from './libs/Utils/Browser';
import { setIsLogged } from './redux/reducers/user';
import DesktopRouters from './routers/router.desktop';

function App() {
  const dispatch = useDispatch();

  if (global.localStorage.getItem('user')) {
    dispatch(setIsLogged(true));
  }

  const isMob: boolean = isMobile();

  return (
    <div className={isMob ? 'app mobile' : 'app'}>
      <DesktopRouters />
    </div>
  );
}

export default App;
